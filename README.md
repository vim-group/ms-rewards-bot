# Microsoft Rewards Bot
<p>Bot for gaining points for Microsoft Rewards doing Bing searches.</p>

<h3>*DISCLAIMER*</H3>
This bot violates the <a href="https://www.microsoft.com/en-us/servicesagreement">Terms of Services</a> of the Microsoft Rewards program. Use it at your own risk.


<h3>Browser supported:</h3>
<ul>
    <li>Edge</li>
    <li>Firefox</li>
    <li>Chrome</li>
</ul>
<h3>OS supported:</h3>
<ul>
    <li>Windows</li>
    <li>Linux (in progress)</li>
    <li>OSX (may come in the future)</li>
</ul>

<h3>Compiling and testing</h3>
<p>Clone 'dev' branch with Visual Studio or Visual Studio Code. </p>
Visual Studio:
<ul>
    <li>Open "MSRewardsBOT.sln"</li>
    <li>'Build' -> 'Build Solution'</li>
    <li>'Debug' -> 'Start with debugging' or 'Start without debugging'</li>
</ul>
Visual Studio Code:
<ul>
    <li>Open the cloned folder. The first time VS Code opens the project, it will download all the needed extensions.</li>
    <li>From the terminal (you can use the integrated one or the Windows Terminal),
    type 'dotnet run'</li>
</ul>
</p>

<h3>Requirements:</h3>

<ul>
    <li>Windows 10: <a href="https://dotnet.microsoft.com/download">.NET v5.0</a></li>
<li>Linux:      <a href="https://docs.microsoft.com/it-it/dotnet/core/install/linux">.NET (core) package v5.0</a></li>
<details>
           <summary>Supported distribution</summary>
           <p>Alpine  CentOS  Debian  Fedora  OpenSUSE  RedHat  SLES Ubuntu</p>
</details>
</ul>
<p>Depeding on your IDE installation, you may have to download some packages (aka extensions).
("HTMLAgilityPack","NuGET Package Manager" come on my mind).</p>
