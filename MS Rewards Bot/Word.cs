﻿using System;
using System.IO;
using System.Reflection;

namespace MS_Rewards_Bot
{
    public class Word
    {
        private bool IsRandom { get; set; }
        private string Result { get; set; }
        public Word(bool isRandom)
        {
            IsRandom = isRandom;
            if (IsRandom)
            {
                Generate();
            }
            else
            {
                Select();
            }
        }
        public override string ToString()
        {
            return Result;
        }
        /**
         * GENERATE FUNCTION
         * Generate a random string of [5-10] characters to use as a search query
         */
        private void Generate()
        {
            var rand_num = new Random();
            string charset = "abcdefghijklmnopqrstuvwxyz";
            string word_query = ""; // Start with an empty string
            Random rand_length = new Random();
            int length = rand_length.Next(5, 15); // Set a random string length (from 5 to 15 chars)

            for (int i = 0; i < length; i++)
            {
                int index = rand_num.Next(charset.Length);
                word_query += charset[index];

            }
            Result = word_query;
        }
        /*
         * SELECT FUNCTION
         * 
         * Select a random query from a list to use as a search query
         * (Said list must be 'populated' by the user) - No more! :)
         * 
         */
        private void Select()
        {
            try
            {
                string fileName = "\\wordlist.txt";
                string filePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + fileName;
                //string filePath = "C:\\wordlist.txt";
                string[] wordList = File.ReadAllLines(filePath);

                var rand = new Random();
                int rand_num = rand.Next(wordList.Length);
                
                Result = wordList[rand_num];
            }
            catch (Exception ex)
            {
                Menu menu = new Menu();
                menu.Error("An error was encountered while opening the file.", ex);
            }
        }
    }
}
