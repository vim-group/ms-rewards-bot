﻿using System.Reflection;

namespace MS_Rewards_Bot
{
    class Program
    {
        public static void Main(string[] args) //args for passing arguments like: programName.exe -auto
        {
            Menu menu = new Menu();

            GetHTMLData data = new GetHTMLData();
            /*
            if (args[0] == "-auto")
            {
                menu.AutoMode();
            }
            */
            menu.GiveMeSomeSpace();
            menu.Msg("BROWSER AUTOMATED SEARCH " + GetVersion());
            menu.Msg("Save your browser work before proceed.");
            //menu.GiveMeSomeSpace();
            //menu.Log("Current points: " + data.GetPoints());
            menu.GiveMeSomeSpace();
            menu.PlatformMenu();
            //menu.AutoMode();
        }
        private static string GetVersion()
        {
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return version;
        }
    }
}
