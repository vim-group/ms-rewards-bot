﻿using System.IO;

namespace MS_Rewards_Bot
{
    public class SetBrowser
    {
        //Private set browser path, public get browser path => if not installed it return: ""
        public string Path { get; private set; }
        public SetBrowser(string selectedBrowser)
        {
            switch (selectedBrowser)
            {
                case "chrome":
                    this.Path = Chrome();
                    break;
                case "firefox":
                    this.Path = Firefox();
                    break;
                case "edge":
                    this.Path = Edge();
                    break;
            }
        }
        private static string Chrome() //need to be private in the future!
        {
            if (System.Environment.Is64BitOperatingSystem == true)
            {
                string GoogleChromeFileName = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                if (File.Exists(GoogleChromeFileName))
                {
                    return GoogleChromeFileName;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                string GoogleChromeFileName = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
                if (File.Exists(GoogleChromeFileName))
                {
                    return GoogleChromeFileName;
                }
                else
                {
                    return "";
                }
            }
        }
        private static string Firefox()
        {
            string FireFoxFileName = @"C:\Program Files\Mozilla Firefox\firefox.exe";
            if (File.Exists(FireFoxFileName))
            {
                return FireFoxFileName;
            }
            else
            {
                return "";
            }
        }
        private static string Edge()
        {
            string EdgeFileName = @"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe";
            if (File.Exists(EdgeFileName))
            {
                return EdgeFileName;
            }
            else
            {
                return "";
            }
        }
    }
}
