﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace MS_Rewards_Bot
{
    public class GetHTMLData
    {
        //private int TotalPoints { get; set; }
        string link = @"https://account.microsoft.com/rewards/pointsbreakdown";
        string XPathPoints = @"//*[@id='userBanner']/mee-banner/div/div/div/div[2]/div[1]/mee-banner-slot-2/mee-rewards-user-status-item/mee-rewards-user-status-balance/div/div/div/div/div/p[1]/mee-rewards-counter-animation/span";
        public string GetPoints()
        {
            HtmlWeb web = new HtmlWeb();

            var htmlDoc = web.Load(link);

            var node = htmlDoc.DocumentNode.SelectSingleNode(XPathPoints);
            //if not found
            if (node == null)
            {
                return "";
            }
            else
            {
                return node.OuterHtml;
            }
            
        }
    }
}
