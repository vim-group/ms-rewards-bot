﻿using System;

namespace MS_Rewards_Bot
{
    public class Menu
    {
        //Platform choice - Menu 1
        public void PlatformMenu()
        {
            GiveMeSomeSpace();
            Log("Choose the platform:");
            Log("1. Desktop --default");
            Log("2. Mobile");

            switch (Console.ReadLine())
            {
                case "1":
                    BrowserMenu(false);
                    break;
                case "2":
                    BrowserMenu(true);
                    break;
                default:
                    InLine("Desktop");
                    BrowserMenu(false);
                    break;
            }
        }
        //Browser choice - Menu 2
        public void BrowserMenu(bool isMobile)
        {
            SetBrowser chrome = new SetBrowser("chrome");
            SetBrowser firefox = new SetBrowser("firefox");
            SetBrowser edge = new SetBrowser("edge");

            GiveMeSomeSpace();
            Log("Choose the browser:");
            Log("The browsers not supported/installed are automatically hidden");

            //Check if browser is installed => If not, it return nothing
            if (edge.Path != "") 
            {
                Log("1. Edge --default");
            }
            if (chrome.Path != "")
            {
                Log("2. Chrome");
            }
            if (firefox.Path != "")
            {
                Log("3. Firefox");
            }
            if (edge.Path == "" && chrome.Path == "" && firefox.Path == "")
            {
                Error("No browser detected.");
                Error("Press 0 to exit.");
            }
            //Read input
            string input = Console.ReadLine();
            
            if (input == "1" && edge.Path != "")
            {
                WordMenu("edge", isMobile);
            }
            else if (input == "2" && chrome.Path != "")
            {
                WordMenu("chrome", isMobile);
            }
            else if (input == "3" && firefox.Path != "")
            {
                WordMenu("firefox", isMobile);
            }else if(input == "0")
            {
                Environment.Exit(-1);
            }
            else
            {
                InLine("Edge");
                WordMenu("edge", isMobile);
            }
        }
        //Word generation choice - Menu 3
        public void WordMenu(string browser,bool isMobile)
        {
            GiveMeSomeSpace();
            Log("Choose the method for generating the words:");
            Log("1. Random --default");
            Log("2. Wordlist");

            switch (Console.ReadLine())
            {
                case "1":
                    SearchesMenu(browser, isMobile, true);
                    break;
                case "2":
                    SearchesMenu(browser, isMobile, false);
                    break;
                default:
                    InLine("Random");
                    SearchesMenu(browser, isMobile, true);
                    break;
            }
        }
        public void SearchesMenu(string browser,bool isMobile,bool isRandom)
        {
            GiveMeSomeSpace();

            int numSearch;

            if (isMobile)
            {
                Question("Set number of mobile searches [1-22]:");
                string answer = Console.ReadLine();

                if (Int32.TryParse(answer,out numSearch))
                {
                    if (numSearch < 1)
                    {
                        Error("Please insert a number greater than 0.");
                        SearchesMenu(browser, isMobile, isRandom);
                    }
                    if (numSearch > 22)
                    {
                        Error("Please insert a number lesser than 22.");
                        SearchesMenu(browser, isMobile, isRandom);
                    }
                }
                else //default
                {
                    numSearch = 20;
                    InLine("20");
                }
            }
            else
            {
                Question("Set number of desktop searches [1-34]:");
                string answer = Console.ReadLine();

                if (Int32.TryParse(answer, out numSearch))
                {
                    if (numSearch < 1)
                    {
                        Error("Please insert a number greater than 0.");
                        SearchesMenu(browser, isMobile, isRandom);
                    }
                    if (numSearch > 34)
                    {
                        Error("Please insert a number lesser than 34.");
                        SearchesMenu(browser, isMobile, isRandom);
                    }
                }
                else //default
                {
                    numSearch = 30;
                    InLine("30");
                }
            }
            //Run the processes
            _ = new Proc(browser, isMobile, isRandom, numSearch);
            Msg("Searches completed successfully.");
            //Return to Main Menu
            PlatformMenu();
        }
        public void AutoMode()
        {
            /**
             * Max Search Desktop: 30
             * Max Search Mobile: 20
             * Only Edge for more points
            */

            Msg("RUNNING IN AUTOMATED MODE");

            Log("Running mobile searches..");
            _ = new Proc("edge", true, false, 20);
            Msg("Mobile searches completed.");

            Log("Running desktop searches..");
            _ = new Proc("edge", false, false, 30);
            Msg("Desktop searches completed.");

            Log("Ended.");
            PlatformMenu();
        }
        public void GiveMeSomeSpace() //And wear the mask!
        {
            Console.WriteLine("\n\r");
        }
        public void Log(string log)
        {
            Console.WriteLine(">> " + log);
        }
        public void Error(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(">> " + error);
            Console.ResetColor();
        }
        //Overload for exception
        public void Error(string error, Exception exception)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(">> " + error);
            Console.WriteLine(exception.Message);
            Console.ResetColor();
        }
        public void Msg(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(">> " + message);
            Console.ResetColor();
        }
        public void Question (string question)
        {
            Console.Write(">> " + question + " ");
        }
        public void InLine(string message)
        {
            Console.Write(">> " + message);
        }
    }
}
