﻿using System;
using System.Diagnostics;
using System.Threading;

namespace MS_Rewards_Bot
{
    public class Proc
    {
        //This class is for the management of the processes
        private string SelectedBrowser { get; set; }
        private bool IsMobile { get; set; }
        private bool IsRandom { get; set; }
        private int NumSearches { get; set; }

        public Proc(string selectedBrowser, bool isMobile, bool isRandom, int numSearches)
        {
            SelectedBrowser = selectedBrowser;
            IsMobile = isMobile;
            IsRandom = isRandom;
            NumSearches = numSearches;
            OpenProc(SelectedBrowser, IsMobile, IsRandom, NumSearches);
        }

        private void OpenProc(string selectedBrowser, bool isMobile,bool isRandom,int numSearches)
        {
            //init
            Menu menu = new Menu();
            //Get selected browser path
            SetBrowser browser = new SetBrowser(selectedBrowser);
            string pathBrowser = browser.Path;

            //Set the args for process
            ProcessStartInfo psi = new ProcessStartInfo(pathBrowser);
            psi.UseShellExecute = true;
            //psi.CreateNoWindow = true;
            psi.WindowStyle = ProcessWindowStyle.Minimized;

            //init local var for counting searches
                //is 1 because is the minimum for searching, otherwise you got an error
            int totSearches = 1; 
            //init obj random sleep
            Random sleep = new Random();

            for (int i = 0; i < NumSearches; i++)
            {
                //URL
                Word searchQuery = new Word(isRandom);
                string url = "https://www.bing.com/search?q=" + searchQuery;
                //Apply to the URL
                string args = BrowserArgs(isMobile) + "\"" + url + "\"";
                psi.Arguments = args;

                psi.WindowStyle = ProcessWindowStyle.Minimized;

                //Open the process
                try
                {
                    Process process = Process.Start(psi);
                }
                catch (Exception ex)
                {
                    menu.Error("An error occurred while opening the process.", ex);
                }

                //Search Log
                Console.WriteLine(">> Done searches: " + totSearches + "/" + NumSearches, (totSearches++));
                Console.SetCursorPosition(0, Console.CursorTop - 1);

                //Do some sleep
                Thread.Sleep(sleep.Next(7000, 15000));

                //If is opened more than 5 processes it be killed -- if he return 0 is multiple of 5
                if ((i % 5) == 0)
                {
                    CloseProc(selectedBrowser);
                }
            }
            //When the method is completed close the remain browser's tab
            CloseProc(selectedBrowser);
        }
        private void CloseProc(string processName)
        {
            if (processName == "edge")
            {
                processName = "msedge";
            }
            try
            {
                if (IsRunning(processName))
                {
                    foreach (var procs in Process.GetProcessesByName(processName))
                    {
                        procs.Kill();
                    }
                }
            }
            catch (System.ComponentModel.Win32Exception) { }
        }
        private bool IsRunning(string processName)
        {
            Process[] pname = Process.GetProcessesByName(processName);
            if (pname.Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string BrowserArgs(bool isMobile)
        {
            //TEST
            //Mobile User Agent
            string mobileUA = "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
            //Desktop User Agent
            //string desktopUA = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";

            string anAwesomeSPACE = " ";
            if (isMobile)
            {
                return "--user-agent=\"" + mobileUA + "\"" + anAwesomeSPACE;
            }
            else
            {
                return "";
            }
        }
    }
}
